﻿using UnityEngine;
using UI;

public class GameController : MonoBehaviour
{
	[SerializeField] private UIMazeView uiMazeView = null;
	[SerializeField] private MazeManager mazeManager = null;
	[SerializeField] private Follower cameraPivot = null;
	[SerializeField] private GameObject playerPrefab = null;
	[SerializeField] private GameObject powerupPrefab = null;
	[SerializeField] private int defaultEnergy = 0;

	private GameObject currentPowerUp = null;
	private PlayerController playerCtrl = null;

	private void Start()
	{
		InstantiatePlayer();
	}

	private void InstantiatePlayer()
	{
		GameObject playerObj = Instantiate(playerPrefab, new Vector3(1f, 0f, 1f), Quaternion.identity);
		playerCtrl = playerObj.GetComponent<PlayerController>();
		playerCtrl.OnPlayerDead += ResetGame;
		playerCtrl.OnPlayerMoved += PlayerMoved;
		playerCtrl.Init(mazeManager, defaultEnergy);

		cameraPivot.SetTarget(playerObj.transform);
	}

	private void OnEnable()
	{
		uiMazeView.OnRandomMazeSubmit += RandomMaze;
		uiMazeView.OnMappedMazeSubmit += MappedMaze;
	}

	private void OnDisable()
	{
		uiMazeView.OnRandomMazeSubmit -= RandomMaze;
		uiMazeView.OnMappedMazeSubmit -= MappedMaze;
	}

	private void ResetGame()
	{
		uiMazeView.Init();
		playerCtrl.OnPlayerDead -= ResetGame;
		playerCtrl.OnPlayerMoved -= PlayerMoved;
		Destroy(playerCtrl.gameObject);

		InstantiatePlayer();
	}

	private void MappedMaze(int width, int height, int[,] grid, WallColor wallColor)
	{
		mazeManager.MapMaze(width, height, grid, wallColor);
		GeneratePowerUp();
	}

	private void RandomMaze(int width, int height, WallColor wallColor)
	{
		mazeManager.Init(width, height, wallColor);
		GeneratePowerUp();
	}

	private void GeneratePowerUp()
	{
		bool generate = true;
		while (generate)
		{
			int x = Random.Range(1, mazeManager.Maze.Width);
			int y = Random.Range(1, mazeManager.Maze.Height);
			if ((x == 1 && y == 1) || mazeManager.Maze.Grid[y, x] == 1) continue;

			Vector3 powerUpPos = new Vector3(x, 0, y);
			if (currentPowerUp != null)
				currentPowerUp.transform.position = powerUpPos;
			else
				currentPowerUp = Instantiate(powerupPrefab, powerUpPos, Quaternion.identity);

			generate = false;
		}
	}

	private void PlayerMoved(Vector3 position)
	{
		if (Mathf.RoundToInt(position.x) != mazeManager.Maze.FarthestCell.X ||
			Mathf.RoundToInt(position.z) != mazeManager.Maze.FarthestCell.Y)
			return;

		Debug.Log("Victory!");
		ResetGame();
	}
}
