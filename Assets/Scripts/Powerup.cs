﻿using UnityEngine;

public class Powerup : MonoBehaviour
{
	[SerializeField] private int freeSteps = 0;

	private void OnTriggerEnter(Collider other)
	{
		if (!other.CompareTag("Player")) return;

		Debug.Log("Power up!");

		other.GetComponent<PlayerController>().PowerShift(transform.position, freeSteps);
		Destroy(gameObject);
	}
}
