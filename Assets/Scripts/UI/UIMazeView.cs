﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace UI
{
	public class UIMazeView : MonoBehaviour
	{
		public event Action<int, int, WallColor> OnRandomMazeSubmit;
		public event Action<int, int, int[,], WallColor> OnMappedMazeSubmit;

		[SerializeField] private TMP_InputField xInput = null;
		[SerializeField] private TMP_InputField yInput = null;
		[SerializeField] private Button applyMapButton = null;
		[SerializeField] private GridLayoutGroup gridLayout = null;
		[SerializeField] private TMP_Dropdown colorChoice = null;
		[SerializeField] private GameObject cellPrefab = null;

		private int[,] grid;
		private WallColor wallColor = WallColor.White;

		public void Init()
		{
			gameObject.SetActive(true);
		}

		public void InputXSubmit()
		{
			SubmitProcess(xInput);
		}

		public void InputYSubmit()
		{
			SubmitProcess(yInput);
		}

		public void SubmitSettings()
		{
			InputXSubmit();
			InputYSubmit();
			int width = int.Parse(xInput.text);
			int height = int.Parse(yInput.text);

			Button[] buttons = gridLayout.GetComponentsInChildren<Button>();
			for (int i = 0; i < buttons.Length; i++)
				Destroy(buttons[i].gameObject);

			gridLayout.constraintCount = width;

			grid = new int[height, width];
			Button[,] buttonsGrid = new Button[height, width];
			for (int y = 0; y < height; y++)
				for (int x = 0; x < width; x++)
				{
					GameObject cellObj = Instantiate(cellPrefab);
					cellObj.transform.SetParent(gridLayout.transform);
					cellObj.transform.localScale = Vector3.one;

					Button button = cellObj.GetComponent<Button>();

					if (x == 0 || y == 0 || x == width - 1 || y == height - 1)
					{
						CellClicked(x, y, button);
						continue;
					}

					buttonsGrid[y, x] = button;
					//we need these variables for delegates to work correctly
					int xCell = x;
					int yCell = y;
					buttonsGrid[y, x].onClick.AddListener(delegate { CellClicked(xCell, yCell, button); });
				}
		}

		public void ApplyMap()
		{
			InputXSubmit();
			InputYSubmit();
			int x = int.Parse(xInput.text);
			int y = int.Parse(yInput.text);
			OnMappedMazeSubmit?.Invoke(x, y, grid, wallColor);
			gameObject.SetActive(false);
		}

		public void GenerateRandomly()
		{
			InputXSubmit();
			InputYSubmit();
			int x = int.Parse(xInput.text);
			int y = int.Parse(yInput.text);
			OnRandomMazeSubmit?.Invoke(x, y, wallColor);
			gameObject.SetActive(false);
		}

		private void Start()
		{
			applyMapButton.interactable = false;
		}

		private void OnEnable()
		{
			colorChoice.onValueChanged.AddListener(delegate { SetColor(colorChoice); });
		}

		private void OnDisable()
		{
			colorChoice.onValueChanged.RemoveAllListeners();
		}

		private void CellClicked(int x, int y, Button button)
		{
			applyMapButton.interactable = true;

			Image image = button.GetComponent<Image>();
			if (grid[y, x] == 0)
			{
				grid[y, x] = 1;
				image.color = Color.grey;
			}
			else
			{
				grid[y, x] = 0;
				image.color = Color.white;
			}
		}

		private void SubmitProcess(TMP_InputField inputField)
		{
			bool isInt = int.TryParse(inputField.text, out int result);

			if (isInt)
			{
				if (result < 5) result = 5;
				if (result % 2 == 0) result++;
			}
			else
				result = 5;

			inputField.SetTextWithoutNotify(result.ToString());
		}

		private void SetColor(TMP_Dropdown colorDropdown)
		{
			wallColor = (WallColor)colorDropdown.value;
		}
	}
}
