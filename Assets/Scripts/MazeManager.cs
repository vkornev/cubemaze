﻿using UnityEngine;

public enum WallColor { White, Red, Green, Blue, Yellow }

public class MazeManager : MonoBehaviour
{
	public Maze Maze { get; private set; }

	[SerializeField] private Transform floor = null;

	public bool IsCellFree(Vector3 cellPosition)
	{
		return Maze.Grid[(int)cellPosition.z, (int)cellPosition.x] != 1;
	}

	public void Init(int width, int height, WallColor wallColor)
	{
		Maze = new Maze(width, height);
		Maze.Generate();
		BuildMaze(width, height, wallColor);
	}

	public void MapMaze(int width, int height, int[,] grid, WallColor wallColor)
	{
		Maze = new Maze(width, height);
		Maze.SetGrid(grid);
		BuildMaze(width, height, wallColor);
	}

	private void BuildMaze(int width, int height, WallColor wallColor)
	{
		Clean();

		floor.localScale = new Vector3(width, 1f, height);
		GameObject cubePrefab = SelectWallPrefab(wallColor);
		for (int y = 0; y < Maze.Height; y++)
			for (int x = 0; x < Maze.Width; x++)
			{
				if (Maze.Grid[y, x] != 1) continue;

				GameObject go = Instantiate(cubePrefab, new Vector3(x, .5f, y), Quaternion.identity);
				go.transform.SetParent(transform);
				go.GetComponent<Collider>();
			}

		Destroy(cubePrefab);
	}

	private static GameObject SelectWallPrefab(WallColor wallColor)
	{
		Color color = Color.white;
		switch (wallColor)
		{
			case WallColor.Red:
				color = Color.red;
				break;
			case WallColor.Green:
				color = Color.green;
				break;
			case WallColor.Blue:
				color = Color.blue;
				break;
			case WallColor.Yellow:
				color = Color.yellow;
				break;
			default:
				break;
		}

		GameObject cubePrefab = GameObject.CreatePrimitive(PrimitiveType.Cube);
		cubePrefab.transform.localScale = new Vector3(1f, .9f, 1f);
		cubePrefab.GetComponent<Renderer>().sharedMaterial.color = color;
		return cubePrefab;
	}

	private void Clean()
	{
		Collider[] cells = GetComponentsInChildren<BoxCollider>();

		for (int i = 0; i < cells.Length; i++)
			Destroy(cells[i].gameObject);
	}
}