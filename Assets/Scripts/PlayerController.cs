﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerController : MonoBehaviour
{
	public event Action<Vector3> OnPlayerMoved;
	public event Action OnPlayerDead;

	public int Energy { get; private set; }

	[SerializeField] private Transform cubePivot = null;
	[SerializeField] private GameObject destruction = null;
	[SerializeField] private float powerShiftDelay = 0f;

	private MazeManager mazeManager = null;
	private bool isMoving = false;

	public void Init(MazeManager mazeManager, int energy)
	{
		this.mazeManager = mazeManager;
		Energy = energy;
	}

	public void PowerShift(Vector3 position, int steps)
	{
		StartCoroutine(PowerShiftCrt(position, steps));
	}

	private void OnEnable()
	{
		InputManager.OnUp += Up;
		InputManager.OnDown += Down;
		InputManager.OnRight += Right;
		InputManager.OnLeft += Left;
	}

	private void OnDisable()
	{
		InputManager.OnUp -= Up;
		InputManager.OnDown -= Down;
		InputManager.OnRight -= Right;
		InputManager.OnLeft -= Left;
	}

	private void Up() => Move(Vector3.forward, Vector3.right);
	private void Down() => Move(-Vector3.forward, -Vector3.right);
	private void Right() => Move(Vector3.right, -Vector3.forward);
	private void Left() => Move(-Vector3.right, Vector3.forward);

	private void Move(Vector3 dir, Vector3 euler)
	{
		if (isMoving || Energy <= 0) return;

		Vector3 nextPosition = transform.position + dir;

		if (!mazeManager.IsCellFree(nextPosition)) return;

		Energy--;

		isMoving = true;
		cubePivot.DOBlendableRotateBy(euler * 90f, .5f).SetEase(Ease.Linear);
		transform.DOMove(nextPosition, .5f).SetEase(Ease.Linear).OnComplete(() => MoveFinished());
	}

	private IEnumerator PowerShiftCrt(Vector3 position, int steps)
	{
		if (mazeManager.Maze.FarthestCell.IsDefault) yield break;

		yield return new WaitForSeconds(powerShiftDelay);

		List<Vector3> path = PathFind.AStar.FindPath(
			mazeManager.Maze.Grid,
			new int[] { Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.z) },
			new int[] { mazeManager.Maze.FarthestCell.X, mazeManager.Maze.FarthestCell.Y });

		//we doin't need our current position
		path.RemoveAt(0);

		if (steps < path.Count) path.RemoveRange(steps - 1, path.Count - (steps - 1));

		MovePath(path);
	}

	private void MoveFinished()
	{
		isMoving = false;
		OnPlayerMoved?.Invoke(transform.position);

		if (Energy <= 0)
			FallApart();
	}

	private void MovePath(List<Vector3> path)
	{
		if (path.Count == 0)
		{
			isMoving = false;
			OnPlayerMoved?.Invoke(transform.position);
			return;
		}

		isMoving = true;
		Vector3 nextPosition = path[0];
		path.RemoveAt(0);
		transform.DOMove(nextPosition, .5f).SetEase(Ease.Linear).OnComplete(() => MovePath(path));
	}

	private void FallApart()
	{
		cubePivot.gameObject.SetActive(false);
		destruction.SetActive(true);
		StartCoroutine(WaitAndReset());
	}

	private IEnumerator WaitAndReset()
	{
		yield return new WaitForSeconds(2f);

		OnPlayerDead?.Invoke();
	}
}
