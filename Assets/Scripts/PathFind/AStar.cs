﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace PathFind
{
	public static class AStar
	{
		public static List<Vector3> FindPath(int[,] maze, int[] start, int[] goal)
		{
			List<PathNode> closedSet = new List<PathNode>();
			List<PathNode> openSet = new List<PathNode>();

			PathNode startNode = new PathNode()
			{
				Position = start,
				CameFrom = null,
				PathLengthFromStart = 0,
				HeuristicEstimatePathLength = GetHeuristicPathLength(start, goal)
			};

			openSet.Add(startNode);
			while (openSet.Count > 0)
			{
				PathNode currentNode = GetBestNode(openSet);
				if (currentNode.IsPositionEqual(goal))
					return GetPathForNode(currentNode);

				openSet.Remove(currentNode);
				closedSet.Add(currentNode);
				foreach (PathNode neighbourNode in GetNeighbours(currentNode, goal, maze))
				{
					bool skip = false;
					for (int i = 0; i < closedSet.Count; i++)
						if (neighbourNode.IsPositionEqual(closedSet[i].Position))
						{
							skip = true;
							break;
						}

					if (skip) continue;

					PathNode openNode = null;
					for (int i = 0; i < openSet.Count; i++)
						if (openSet[i].IsPositionEqual(neighbourNode.Position))
						{
							openNode = openSet[i];
							break;
						}

					if (openNode == null)
						openSet.Add(neighbourNode);
					else if (openNode.PathLengthFromStart > neighbourNode.PathLengthFromStart)
					{
						openNode.CameFrom = currentNode;
						openNode.PathLengthFromStart = neighbourNode.PathLengthFromStart;
					}
				}
			}

			return null;
		}

		private static PathNode GetBestNode(List<PathNode> nodes)
		{
			float f = nodes[0].EstimateFullPathLength;
			PathNode result = nodes[0];
			for (int i = 1; i < nodes.Count; i++)
			{
				if (nodes[i].EstimateFullPathLength < f)
				{
					f = nodes[i].EstimateFullPathLength;
					result = nodes[i];
				}
			}
			return result;
		}

		private static int GetHeuristicPathLength(int[] from, int[] to)
		{
			return Mathf.Abs(to[0] - from[0]) + Mathf.Abs(to[1] - from[1]);
		}

		private static List<PathNode> GetNeighbours(PathNode pathNode, int[] goal, int[,] maze)
		{
			List<PathNode> result = new List<PathNode>();

			List<int[]> neighbourPoints = new List<int[]>
			{
				new int[2]{ pathNode.Position[0] + 1, pathNode.Position[1] },
				new int[2]{ pathNode.Position[0] - 1, pathNode.Position[1] },
				new int[2]{ pathNode.Position[0], pathNode.Position[1] + 1 },
				new int[2]{ pathNode.Position[0], pathNode.Position[1] - 1 }
			};

			List<KeyValuePair<int[], bool>> squareTilesStatus = new List<KeyValuePair<int[], bool>>();
			// process up/down/left/right tiles
			for (int i = 0; i < 4; i++)
			{
				bool occupied = IsObstacle(maze, neighbourPoints[i]);

				squareTilesStatus.Add(new KeyValuePair<int[], bool>(neighbourPoints[i], occupied));

				if (occupied) continue;

				PathNode neighbourNode = new PathNode()
				{
					Position = neighbourPoints[i],
					CameFrom = pathNode,
					PathLengthFromStart = pathNode.PathLengthFromStart + Distance(pathNode.Position, neighbourPoints[i]),
					HeuristicEstimatePathLength = GetHeuristicPathLength(neighbourPoints[i], goal)
				};
				result.Add(neighbourNode);
			}

			return result;
		}

		private static bool IsObstacle(int[,] colliderMaps, int[] neighborPoint)
		{
			bool result = false;

			try
			{
				result = colliderMaps[neighborPoint[1], neighborPoint[0]] == 1;
			}
			catch (Exception)
			{
				result = true;
				Debug.LogWarning("Out of bounds attempt");
			}

			return result;
		}

		private static List<Vector3> GetPathForNode(PathNode pathNode)
		{
			List<Vector3> result = new List<Vector3>();
			PathNode currentNode = pathNode;
			while (currentNode != null)
			{
				result.Add(new Vector3(currentNode.Position[0], 0f, currentNode.Position[1]));
				currentNode = currentNode.CameFrom;
			}
			result.Reverse();
			return result;
		}

		private static float Distance(int[] start, int[] end)
		{
			int vectx = end[0] - start[0];
			int vecty = end[1] - start[1];
			return vectx * vectx + vecty * vecty;
		}
	}
}
