﻿namespace PathFind
{
	public class PathNode
	{
		// Coordinates on map
		public int[] Position { get; set; }
		// Previous node
		public PathNode CameFrom { get; set; }
		// Length from the start (G)
		public float PathLengthFromStart { get; set; }
		// Approximated distance to the end (H)
		public float HeuristicEstimatePathLength { get; set; }
		// Approximated full path length (F)
		public float EstimateFullPathLength
		{
			get
			{
				return PathLengthFromStart + HeuristicEstimatePathLength;
			}
		}

		public bool IsPositionEqual(int[] otherPosition)
		{
			if (Position.Length != otherPosition.Length)
				return false;

			for (int i = 0; i < Position.Length && i < otherPosition.Length; i++)
				if (Position[i] != otherPosition[i])
					return false;

			return true;
		}
	}
}
