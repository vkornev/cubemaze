﻿using UnityEngine;

public class Follower : MonoBehaviour
{
	private Transform target;

	public void SetTarget(Transform target)
	{
		this.target = target;
	}

	private void Update()
	{
		if (!target) return;

		transform.position = Vector3.Lerp(transform.position, target.position, 5f * Time.deltaTime);
	}
}
