﻿using System.Collections.Generic;

public struct Cell
{
	public int X { get; private set; }
	public int Y { get; private set; }
	public bool IsDefault => X == 0 && Y == 0;

	public Cell(int x, int y)
	{
		X = x;
		Y = y;
	}

	public override string ToString()
	{
		return $"{X}, {Y}";
	}
}

public class Maze
{
	public int[,] Grid { get; private set; }
	public int Width { get; private set; }
	public int Height { get; private set; }
	public Cell FarthestCell { get; private set; }

	private const int VISITED_CELL = 2;

	public Maze()
	{
		Width = 0;
		Height = 0;
		Grid = null;
	}

	public Maze(int width, int height)
	{
		Init(width, height);
	}

	public override string ToString()
	{
		string str = string.Empty;
		for (int y = 0; y < Height; y++)
		{
			for (int x = 0; x < Width; x++)
				str += Grid[y, x];

			str += "\n";
		}

		return str;
	}

	public void Init(int width, int height)
	{
		Width = width % 2 == 0 ? width + 1 : width;
		Height = height % 2 == 0 ? height + 1 : height;
		Grid = new int[Height, Width];

		//init maze
		for (int y = 0; y < Height; y++)
			for (int x = 0; x < Width; x++)
				Grid[y, x] = x % 2 != 0 && y % 2 != 0 ? 0 : 1;
	}

	public void SetGrid(int[,] grid)
	{
		Grid = grid;
	}

	public void Generate()
	{
		Stack<Cell> stack = new Stack<Cell>();
		Cell currentCell = new Cell(1, 1);

		int stackMaxLength = 0;
		do
		{
			Grid[currentCell.Y, currentCell.X] = VISITED_CELL;

			Cell[] neighbors = GetUnvisitedNeighbors(currentCell);

			if (neighbors.Length == 0)
			{
				if (stack.Count > stackMaxLength)
				{
					stackMaxLength = stack.Count;
					FarthestCell = currentCell;
				}
				currentCell = stack.Pop();
				continue;
			}

			Cell randomCell = neighbors[UnityEngine.Random.Range(0, neighbors.Length)];

			int wallToRemoveX = (int)((randomCell.X + currentCell.X) * .5f);
			int wallToRemoveY = (int)((randomCell.Y + currentCell.Y) * .5f);

			Grid[wallToRemoveY, wallToRemoveX] = 0;

			stack.Push(currentCell);

			currentCell = randomCell;
		}
		while (stack.Count > 0);

		//reset all visited cells to default value
		for (int y = 0; y < Height; y++)
			for (int x = 0; x < Width; x++)
				if (Grid[y, x] == VISITED_CELL) Grid[y, x] = 0;
	}

	private Cell[] GetUnvisitedNeighbors(Cell cell)
	{
		List<Cell> result = new List<Cell>();

		if (cell.X - 2 > 0 && Grid[cell.Y, cell.X - 2] != VISITED_CELL)
			result.Add(new Cell(cell.X - 2, cell.Y));

		if (cell.Y - 2 > 0 && Grid[cell.Y - 2, cell.X] != VISITED_CELL)
			result.Add(new Cell(cell.X, cell.Y - 2));

		if (cell.X + 2 < Width && Grid[cell.Y, cell.X + 2] != VISITED_CELL)
			result.Add(new Cell(cell.X + 2, cell.Y));

		if (cell.Y + 2 < Height && Grid[cell.Y + 2, cell.X] != VISITED_CELL)
			result.Add(new Cell(cell.X, cell.Y + 2));

		return result.ToArray();
	}
}
