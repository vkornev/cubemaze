﻿using System;
using UnityEngine;

public class InputManager : MonoBehaviour
{
	public static event Action OnEscapePressed;
	public static event Action OnUp;
	public static event Action OnDown;
	public static event Action OnRight;
	public static event Action OnLeft;

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
			OnEscapePressed?.Invoke();

		if (Input.GetKeyDown(KeyCode.UpArrow))
			OnUp?.Invoke();
		if (Input.GetKeyDown(KeyCode.DownArrow))
			OnDown?.Invoke();
		if (Input.GetKeyDown(KeyCode.RightArrow))
			OnRight?.Invoke();
		if (Input.GetKeyDown(KeyCode.LeftArrow))
			OnLeft?.Invoke();
	}
}
